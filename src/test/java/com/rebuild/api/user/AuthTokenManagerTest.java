/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.api.user;

import com.rebuild.TestSupport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class AuthTokenManagerTest extends TestSupport {

    @SuppressWarnings("deprecation")
    @Test
    void accessToken() {
        
        String accessToken = AuthTokenManager.generateToken(SIMPLE_USER, 3, null);

        
        Assertions.assertNotNull(AuthTokenManager.verifyToken(accessToken));
        Assertions.assertNotNull(AuthTokenManager.verifyToken(accessToken));

        
        AuthTokenManager.refreshAccessToken(accessToken);

        
        AuthTokenManager.verifyToken(accessToken, true, true);
        Assertions.assertNull(AuthTokenManager.verifyToken(accessToken));
    }

    @Test
    void onceToken() {
        
        String onceToken = AuthTokenManager.generateOnceToken(null);

        
        Assertions.assertNotNull(AuthTokenManager.verifyToken(onceToken));
        Assertions.assertNull(AuthTokenManager.verifyToken(onceToken));
    }
}
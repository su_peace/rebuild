package com.rebuild.core;

import org.springframework.core.Ordered;


public interface Initialization extends Ordered {

    
    void init() throws Exception;

    @Override
    default int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}

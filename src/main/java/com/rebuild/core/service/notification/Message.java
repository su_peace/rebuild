/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.notification;

import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.privileges.UserService;
import com.rebuild.core.support.i18n.Language;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Setter(AccessLevel.NONE)
public class Message {

    
    public static final int TYPE_DEFAULT = 0;
    
    public static final int TYPE_ASSIGN = 10;
    
    public static final int TYPE_SAHRE = 11;
    
    public static final int TYPE_APPROVAL = 20;
    
    public static final int TYPE_FEEDS = 30;
    
    public static final int TYPE_PROJECT = 40;

    private ID fromUser;
    private ID toUser;
    private String message;
    private int type;
    private ID relatedRecord;

    
    public Message(ID fromUser, ID toUser, String message, int type, ID relatedRecord) {
        this.fromUser = fromUser == null ? UserService.SYSTEM_USER : fromUser;
        this.toUser = toUser;
        this.message = message;
        this.type = type;
        this.relatedRecord = relatedRecord;
    }

    
    public String getTitle4Type() {
        if (this.type == TYPE_ASSIGN) return Language.L("记录分配通知");
        else if (this.type == TYPE_SAHRE) return Language.L("记录共享通知");
        else if (this.type == TYPE_APPROVAL) return Language.L("记录审核通知");
        else if (this.type == TYPE_FEEDS) return Language.L("动态通知");
        else if (this.type == TYPE_PROJECT) return Language.L("项目任务通知");
        else return Language.L("新通知");
    }
}

/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.datareport;

import cn.devezhao.commons.excel.Cell;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.dialect.FieldType;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.utils.ExcelUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TemplateExtractor {

    
    public static final String NROW_PREFIX = ".";
    
    public static final String APPROVAL_PREFIX = NROW_PREFIX + "approval";

    
    public static final String PLACEHOLDER = "__";
    
    protected static final String PH__KEEP = PLACEHOLDER + "KEEP";
    
    protected static final String PH__NUMBER = PLACEHOLDER + "NUMBER";
    
    protected static final String PH__CURRENTUSER = PLACEHOLDER + "CURRENTUSER";
    
    protected static final String PH__CURRENTBIZUNIT = PLACEHOLDER + "CURRENTBIZUNIT";
    
    protected static final String PH__CURRENTDATE = PLACEHOLDER + "CURRENTDATE";
    
    protected static final String PH__CURRENTDATETIME = PLACEHOLDER + "CURRENTDATETIME";

    
    protected static final Pattern PATT_V2 = Pattern.compile("\\{(.*?)}");

    final protected File templateFile;
    final private boolean isListType;

    
    public TemplateExtractor(File template) {
        this(template, Boolean.FALSE);
    }

    
    public TemplateExtractor(File templateFile, boolean isList) {
        this.templateFile = templateFile;
        this.isListType = isList;
    }

    
    public Map<String, String> transformVars(Entity entity) {
        final Set<String> vars = extractVars();

        Entity detailEntity = this.isListType ? null : entity.getDetailEntity();
        Entity approvalEntity = MetadataHelper.hasApprovalField(entity)
                ? MetadataHelper.getEntity(EntityHelper.RobotApprovalStep) : null;

        Map<String, String> map = new HashMap<>();
        for (final String varName : vars) {
            
            if (varName.startsWith(NROW_PREFIX)) {
                String listField = varName.substring(1);

                
                if (varName.startsWith(APPROVAL_PREFIX)) {
                    String stepNodeField = listField.substring(APPROVAL_PREFIX.length());
                    if (approvalEntity != null && MetadataHelper.getLastJoinField(approvalEntity, stepNodeField) != null) {
                        map.put(varName, stepNodeField);
                    } else {
                        map.put(varName, null);
                    }
                }
                
                else if (detailEntity != null) {
                    if (MetadataHelper.getLastJoinField(detailEntity, listField) != null) {
                        map.put(varName, listField);
                    } else {
                        map.put(varName, transformRealField(detailEntity, listField));
                    }
                }
                
                else if (MetadataHelper.getLastJoinField(entity, listField) != null) {
                    map.put(varName, listField);
                } else {
                    map.put(varName, transformRealField(entity, listField));
                }

            } else if (MetadataHelper.getLastJoinField(entity, varName) != null) {
                map.put(varName, varName);
            } else {
                map.put(varName, transformRealField(entity, varName));
            }
        }
        return map;
    }

    
    protected Set<String> extractVars() {
        List<Cell[]> rows = ExcelUtils.readExcel(templateFile);

        Set<String> vars = new LinkedHashSet<>();
        for (Cell[] row : rows) {
            for (Cell cell : row) {
                if (cell.isEmpty()) continue;

                
                

                String cellText = cell.asString();
                Matcher matcher = PATT_V2.matcher(cellText);
                while (matcher.find()) {
                    String varName = matcher.group(1);
                    if (StringUtils.isNotBlank(varName)) vars.add(varName);
                }
            }
        }
        return vars;
    }

    
    protected String transformRealField(Entity entity, String fieldPath) {
        if (TemplateExtractor33.isPlaceholder(fieldPath)) return null;

        if (fieldPath.contains("$")) {
            fieldPath = fieldPath.replace("$", ".");
            if (MetadataHelper.getLastJoinField(entity, fieldPath) != null) {
                return fieldPath;
            }
        }

        String[] paths = fieldPath.split("\\.");
        List<String> realPaths = new ArrayList<>();

        Field lastField;
        Entity father = entity;
        for (String field : paths) {
            if (father == null) return null;

            lastField = findFieldByLabel(father, field);
            if (lastField == null) return null;

            realPaths.add(lastField.getName());

            if (lastField.getType() == FieldType.REFERENCE) {
                father = lastField.getReferenceEntity();
            } else {
                father = null;
            }
        }
        return StringUtils.join(realPaths, ".");
    }

    private Field findFieldByLabel(Entity entity, String fieldLabel) {
        for (Field field : entity.getFields()) {
            if (EasyMetaFactory.getLabel(field).equalsIgnoreCase(fieldLabel)) {
                return field;
            }
        }
        return null;
    }
}

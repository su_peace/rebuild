/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.metadata.easymeta;

import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.persist4j.Field;
import com.rebuild.core.metadata.impl.EasyFieldConfigProps;
import com.rebuild.core.support.general.FieldValueHelper;
import org.apache.commons.lang.StringUtils;

import java.util.Date;


public class EasyDateTime extends EasyField {
    private static final long serialVersionUID = 3882003543084097603L;

    protected EasyDateTime(Field field, DisplayType displayType) {
        super(field, displayType);
    }

    
    public Object convertCompatibleValue(Object value, EasyField targetField, String valueExpr) {
        
        if (StringUtils.isNotBlank(valueExpr)) {
            Date newDate = FieldValueHelper.parseDateExpr("{NOW" + valueExpr + "}", (Date) value);
            if (newDate != null) {
                value = newDate;
            }
        }

        DisplayType targetType = targetField.getDisplayType();
        boolean is2Text = targetType == DisplayType.TEXT || targetType == DisplayType.NTEXT;
        if (is2Text) {
            return wrapValue(value);
        }

        String dateValue = (String) wrapValue(value);
        
        dateValue += "1970-01-01 00:00:00".substring(dateValue.length());

        return CalendarUtils.parse(dateValue);
    }

    @Override
    public Object convertCompatibleValue(Object value, EasyField targetField) {
        return convertCompatibleValue(value, targetField, null);
    }

    @Override
    public Object exprDefaultValue() {
        String valueExpr = (String) getRawMeta().getDefaultValue();
        if (StringUtils.isBlank(valueExpr)) return null;

        
        if (valueExpr.contains("NOW")) {
            return FieldValueHelper.parseDateExpr(valueExpr, null);
        }
        
        else {
            String format = "yyyy-MM-dd HH:mm:ss".substring(0, valueExpr.length());
            return CalendarUtils.parse(valueExpr, format);
        }
    }

    @Override
    public Object wrapValue(Object value) {
        String format = StringUtils.defaultIfBlank(
                getExtraAttr(EasyFieldConfigProps.DATETIME_FORMAT), getDisplayType().getDefaultFormat());
        return CalendarUtils.getDateFormat(format).format(value);
    }
}

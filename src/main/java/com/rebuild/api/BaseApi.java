/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.api;

import com.alibaba.fastjson.JSON;


public abstract class BaseApi extends Controller {

    protected BaseApi() {
        super();
    }

    
    protected String getApiName() {
        String apiName = getClass().getSimpleName();
        apiName = apiName.replaceAll("[A-Z]", "-$0").toLowerCase();
        return apiName.substring(1);
    }

    
    abstract public JSON execute(ApiContext context) throws ApiInvokeException;
}
